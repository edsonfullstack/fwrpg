﻿using System;
using MongoDB.Bson;
using MongoDB.Driver;

namespace Database.Model
{
    [Serializable]
    public class Account
    {
        #region Variables    
        public ObjectId _id { set; get; }
        public int ActiveConnection { set; get; }
        public string UserName { set; get; }
        public string Email { set; get; }
        public string ShaPassword { set; get; }
        public string Type { set; get; }
        #endregion
        #region UnityMethods

        #endregion
        #region Methods
        public Account()
        {
            this.UserName = "Set";
            this.Type = "Normal";
            this.Email = "edson.azk@gmail.com";
            this.ShaPassword = "668262";
        }
        public Account(string userName, string email, string shaPassword)
        {
            this.UserName = userName;
            this.Email = email;
            this.ShaPassword = shaPassword;
        }
        #endregion
    }
}