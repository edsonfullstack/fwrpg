﻿using System;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Driver;
using UnityEngine;

namespace Database.Model
{
    [Serializable]
    public class Character
    {
        #region Variables
        public ObjectId _id { set; get; }
        public ObjectId User { set; get; }
        public string Name { set; get; }
        public int Level { set; get; }
        public string Class { set; get; }
        [SerializeField] public IDictionary<string, int[]> stats;
        public Tuple<ObjectId, string, int>[,] itens;
        #endregion
        #region UnityMethods

        #endregion
        #region Methods
        public Character()
        {
            this.User = new ObjectId("5dd5dc44bc35c031c80b4779");
            this.Name = "Set";
            this.Level = 30;
            this.Class = "Programmer";
            this.stats = new Dictionary<string, int[]>(){
            { "Strength",new int[]{0,10}}
        };
            itens = new Tuple<ObjectId, string, int>[10, 10];
            int quantidade = 99;
            this.itens[0, 9] = Tuple.Create(new ObjectId("5dd5dc44bc35c031c80b4779"), "mods", quantidade);
        }
        #endregion
    }
}