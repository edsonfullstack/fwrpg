﻿using UnityEngine;
using MongoDB.Bson;
using MongoDB.Driver;
using System.Collections;
using System.Collections.Generic;
using System;
using Database.Model;

namespace Database
{


    public class Mongo
    {
        #region Variables
        private const string MONGO_URI = "mongodb+srv://master:668262az@world0-o28vw.gcp.mongodb.net/test?retryWrites=true&w=majority";
        private const string DATABASE_NAME = "world";
        private MongoClient client;
        private IMongoDatabase db;
        [SerializeField] Account account;
        [SerializeField] Character character;

        #endregion
        #region UnityMethods
        /*  void Start()
           {
               this.Init();
           }
           void Update()
           {
               if (Input.GetKeyDown(KeyCode.Space))
               {

                   account = new Account();
                   this.Insert("account", account);
                   character = new Character();
                   this.Insert("character", character);
               }
               if (Input.GetKeyDown(KeyCode.F))
               {
                   account = new Account();
                   int p = this.Count(collection: "account", q: "{ 'UserName': 'Set', 'Type' : 'Normal' }", format: account);
                   print(p);
               }
               if (Input.GetKeyDown(KeyCode.G))
               {
                   //account = new Account();
                   account = this.Find(collection: "account", q: "{ 'UserName': 'Set', 'Type' : 'Normal' }", format: account);
                   print(account.ToJson());
                   //character = new Character();
                   character = this.Find(collection: "character", q: "{ 'Name': 'Set'}", format: character);
                   print(character.ToJson());
               }
               if (Input.GetKeyDown(KeyCode.V))
               {
                   account = this.Find(collection: "account", q: "{ 'UserName': 'Set', 'Type' : 'Normal' }", format: account);
                   account.ShaPassword = "123456";
                   this.Update(collection: "account", format: account);
                   character = this.Find(collection: "character", q: "{ 'Name': 'Set'}", format: character);
                   character.itens[0, 0] = Tuple.Create(new ObjectId("5dd5dc44bc35c031c80b4779"), "mods", 1);
                   character.stats["Strength"] = new[] { 11, 90 };
                   this.Update("character", character);
                   print(character.stats["Strength"][0]);
                   print((character.itens.ToJson()));
               }
           }
           void OnDestroy()
           {
               this.ShutDown();
           }*/
        #endregion
        #region Methods
        public void Init()
        {
            client = new MongoClient(MONGO_URI);
            db = client.GetDatabase(DATABASE_NAME);
        }
        public void ShutDown()
        {
            client = null;
            db = null;
        }
        #region Insert
        //TData need be the object it will be add,so every model need have a contruct and reach as a object (where TData : new())
        public void Insert<TData>(string collection, TData entity)
        {
            IMongoCollection<TData> data = db.GetCollection<TData>(collection);
            data.InsertOne(entity);
        }
        #endregion
        #region Fetch
        public TData Find<TData>(string collection, string q, TData format)
        {
            IMongoCollection<TData> data = db.GetCollection<TData>(collection);
            BsonDocument filter = BsonDocument.Parse(q);
            TData model = data.Find<TData>(filter).SingleOrDefault();
            return model;
        }
        public List<TData> FindAll<TData>(string collection, string q, TData format)
        {
            IMongoCollection<TData> data = db.GetCollection<TData>(collection);
            BsonDocument filter = BsonDocument.Parse(q);
            List<TData> modelList = data.Find<TData>(filter).ToList();
            return modelList;
        }
        public int Count<TData>(string collection, string q, TData format)
        {
            this.Init();
            IMongoCollection<TData> data = db.GetCollection<TData>(collection);
            BsonDocument filter = BsonDocument.Parse(q);
            List<TData> modelList = data.Find<TData>(filter).ToList();
            return modelList.Count;
        }

        #endregion
        #region Update
        //this.FindAndReplace(collection: "accounts", q: "{ 'UserName': 'Set', 'Type' : 'Normal' }", updateKey: "Type", updateValue: "Update");
        public void Update<TData>(string collection, TData format)
        {
            IMongoCollection<TData> data = db.GetCollection<TData>(collection);
            BsonDocument query = new BsonDocument { { "_id", format.ToBsonDocument().GetValue("_id") } };
            var result = data.FindOneAndReplace(query, format);
        }
        public void UpdateField<TData>(string collection, string q, TData format, string updateKey, string updateValue)
        {
            IMongoCollection<TData> data = db.GetCollection<TData>(collection);
            var result = data.UpdateOne(q, Builders<TData>.Update.Set(updateKey, updateValue));
        }
        public void UpdateAllFields<TData>(string collection, string q, TData format, string updateKey, string updateValue)
        {
            IMongoCollection<TData> data = db.GetCollection<TData>(collection);
            var result = data.UpdateMany(q, Builders<TData>.Update.Set(updateKey, updateValue));
        }
        #endregion
        #region Delete
        public void Delete<TData>(string collection, string q, TData format)
        {
            IMongoCollection<TData> data = db.GetCollection<TData>(collection);
            var result = data.DeleteOne(q);
        }
        public void DeleteAll<TData>(string collection, string q, TData format)
        {
            IMongoCollection<TData> data = db.GetCollection<TData>(collection);
            var result = data.DeleteMany(q);
        }
        #endregion
        #endregion
    }
}