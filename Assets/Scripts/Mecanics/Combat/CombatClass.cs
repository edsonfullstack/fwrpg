﻿using System;
using System.Collections;
using System.Collections.Generic;
using MongoDB.Bson;
using UnityEngine;
using UnityEngine.AI;

namespace Mecanics.Combat
{
    public class CombatClass : MonoBehaviour
    {
        #region Variables
        [SerializeField] float range = 1.5f;
        [SerializeField] float baseRange = 1.5f;
        [SerializeField] Nullable<decimal> inRange = 100m;
        [SerializeField] Transform target = null;
        [SerializeField] float speed;
        NavMeshAgent navMeshAgent;
        #endregion
        #region UnityMethods
        void Start()
        {
            navMeshAgent = GetComponent<NavMeshAgent>();
        }
        void Update()
        {
            if (target != null)
            {

                print($"position:{target.position} -in Range:" + inRange);
                MoveTo(target.gameObject.transform.position);
                Attack(target.gameObject);

            }

            UpdateAnimator();
        }
        #endregion
        #region Methods
        public void Target(GameObject combatTarget)
        {
            target = combatTarget.transform;
        }
        public void Attack(GameObject combatTarget)
        {
            inRange = (decimal)Vector3.Distance(transform.position, target.position);
            if (inRange < (decimal)(range + baseRange) && inRange != null)
            {
                target = combatTarget.transform;
                print("Atacar:" + target.name);
                Stop();
            }
        }
        public void MoveTo(Vector3 destination)
        {
            navMeshAgent.isStopped = true;
            navMeshAgent.isStopped = false;
            navMeshAgent.destination = destination;
        }
        public void Stop()
        {
            navMeshAgent.isStopped = true;
            inRange = null;
            target = null;
        }
        public void Cancel()
        {
            target = null;
        }
        public void UpdateAnimator()
        {
            Vector3 velocity = navMeshAgent.velocity;
            Vector3 localVelocity = transform.InverseTransformDirection(velocity);
            speed = localVelocity.z;
            GetComponent<Animator>().SetFloat("forwardSpeed", speed);
        }
    }
    #endregion
}