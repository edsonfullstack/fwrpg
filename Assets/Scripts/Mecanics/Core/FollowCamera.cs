﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Mecanics.Core
{
    [ExecuteInEditMode]
    public class FollowCamera : MonoBehaviour
    {
        #region Variables
        [SerializeField] Transform target;
        #endregion
        #region UnityMethods
        void LateUpdate()
        {
            transform.position = target.position;
        }
        #endregion
        #region Methods

        #endregion
    }

}