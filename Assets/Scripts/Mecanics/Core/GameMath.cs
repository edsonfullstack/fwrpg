﻿using UnityEngine;


namespace Mecanics.Core
{

    public class GameMath
    {
        #region Variables
        public int value;
        public int numberOfDiceValue;
        #endregion
        #region UnityMethods

        #endregion
        #region Methods
        public GameMath()
        {

        }
        public int DiceRoll(int min, int max, int add = 0, int numberOfDices = 1)
        {
            value = Random.Range(min, max + 1) + add;
            numberOfDiceValue = value * numberOfDices;
            if (value < min + add || value > max + add)
            {
                ErrrorCall("DiceRoll:(" + numberOfDices + "d(" + min + ")(" + max + ")(" + add + ")");
            }
            Debug.Log("You rolled bettew (" + numberOfDices + "d (" + min + " and " + max + "+" + add + "))");
            return numberOfDiceValue;
        }

        private void ErrrorCall(string error)
        {
            Debug.Log(error);
        }
        #endregion


    }
}