﻿using System.Text.RegularExpressions;
using System.Linq;
using System;
using System.Text;
using System.Security.Cryptography;

namespace Mecanics.Core
{
    public class Utility
    {
        #region Variables
        public const string EMAIL_PATTERN = @"^[\w!#$%&'*+\-/=?\^_`{|}~]+(\.[\w!#$%&'*+\-/=?\^_`{|}~]+)*" + "@" + @"((([\-\w]+\.)+[a-zA-Z]{2,4})|(([0-9]{1,3}\.){3}[0-9]{1,3}))$";
        public const string USERNAME_AND_DISCRIMINATOR_PATTERN = @"^[a-zA-Z0-9]{4,20}#[0-9]{4}$";
        public const string USERNAME_PATTERN = @"^[a-zA-Z0-9]{4,20}$";
        public const string RANDOM_CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        #endregion
        #region UnityMethods

        #endregion
        #region Methods
        public static bool IsEmailValid(string email)
        {
            if (email != null)
            {
                return Regex.IsMatch(email, EMAIL_PATTERN);
            }
            else
            {
                return false;
            }

        }
        public static bool IsUserNameValid(string userName)
        {
            if (userName != null)
            {
                return Regex.IsMatch(userName, USERNAME_PATTERN);
            }
            else
            {
                return false;
            }
        }
        public static string GenerateRandomToken(int lenght)
        {
            Random r = new Random();
            return new string(Enumerable.Repeat(RANDOM_CHARS, lenght).Select(s => s[r.Next(s.Length)]).ToArray());
        }
        public static string Sha256FromString(string toEncrypt)
        {
            var mensage = Encoding.UTF8.GetBytes(toEncrypt);
            SHA256Managed hashString = new SHA256Managed();
            string hex = "";
            var hashValue = hashString.ComputeHash(mensage);
            foreach (var x in hashValue)
            {
                hex += String.Format("{0:x2}", x);
            }

            return hex;
        }
        #endregion
    }
}