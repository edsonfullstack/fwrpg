﻿using UnityEngine;
using Mecanics.Combat;
using System;

namespace Player.Control
{
    public interface IPlayerControllerPointClick
    {
        void ActionMaker(GameObject obj);
        void DebugRay(Ray ray);
        GameObject GetMouseInput();
        GameObject GetMouseInput2();
        Ray GetMouseRay();
        void Interact(GameObject obj);
        void Move();
        void ResetMouseInput();
        int Test();
    }

    public class PlayerControllerPointClick : MonoBehaviour, IPlayerControllerPointClick
    {
        #region Variables
        public string servermode = "debug";
        public GameObject target = null;
        public bool hasPosition = false;
        public RaycastHit position;
        CombatClass combatClass;

        #endregion
        #region UnityMethods
        void Start()
        {
            combatClass = GetComponent<CombatClass>();
        }
        void Update()
        {

            target = GetMouseInput();
            if (target)
            {
                ActionMaker(target);
            }
        }
        #endregion
        #region Methods
        public void ActionMaker(GameObject obj)
        {
            DebugMemsage($"pos:{transform.position}target({obj.transform.name}){obj.transform.position}Lay:{obj.layer}");
            if (obj.layer == 8)
            {
                Move();
            }
            else
            {
                Interact(obj);
            }
            ResetMouseInput();
        }
        public int Test()
        {
            return 100;
        }
        private void DebugMemsage(string text)
        {
            if (servermode == "debug")
            {
                print("debug mensage:" + text);
            }
        }
        public void ResetMouseInput()
        {
            target = null;
            hasPosition = false;
        }
        public GameObject GetMouseInput2()
        {
            bool hit = Physics.Raycast(GetMouseRay());
            if (hit)
            {
                if (Input.GetMouseButtonDown(0))
                {
                    return position.transform.gameObject;
                }
            }
            return null;
        }
        public GameObject GetMouseInput()
        {
            RaycastHit[] hits = Physics.RaycastAll(GetMouseRay());
            System.Array.Sort(hits, (x, y) => x.distance.CompareTo(y.distance));
            if (hits.Length > 0)
            {
                if (Input.GetMouseButtonDown(0))
                {
                    return hits[0].transform.gameObject;
                }
            }
            return null;
        }
        public Ray GetMouseRay()
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            hasPosition = Physics.Raycast(ray, out position);
            DebugRay(ray);
            return ray;
        }
        public void DebugRay(Ray ray)
        {
            if (servermode == "debug" && Input.GetMouseButton(0))
            {
                UnityEngine.Debug.DrawRay(ray.origin, ray.direction * 100, Color.red);
            }
        }
        public void Interact(GameObject obj)
        {
            combatClass.Target(obj);
        }
        public void Move()
        {
            combatClass.Cancel();
            combatClass.MoveTo(position.point);
        }
    }
    #endregion
}
