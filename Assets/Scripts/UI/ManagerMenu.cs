﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.UI;
using TMPro;

public class ManagerMenu : MonoBehaviourPunCallbacks
{
    [SerializeField]
    private GameObject NameScreen, ConnectScreen;
    [SerializeField]
    private GameObject CreateUserButton;
    [SerializeField]
    private TMP_InputField NameInputField, PasswordInputField, CreateRoomInputField, JoinRoomInputField;

    private void Awake()
    {
        PhotonNetwork.ConnectUsingSettings();
    }
    public override void OnConnectedToMaster()
    {
        //base.OnConnectedToMaster();
        Debug.Log("Connected to Master!!!");
        PhotonNetwork.JoinLobby(TypedLobby.Default);
    }
    public override void OnJoinedLobby()
    {
        //base.OnJoinedLobby();
        Debug.Log("Connected to Lobby!!!");
        NameScreen.SetActive(true);
    }
    //region UImwthods
    public void OnClickCreateNameButton()
    {
        //checar autenticação
        PhotonNetwork.NickName = NameInputField.text + PasswordInputField.text;
        NameScreen.SetActive(false);
        ConnectScreen.SetActive(true);
    }
    public void OnNameField_Changed()
    {
        if ((NameInputField.text.Length >= 3) && (PasswordInputField.text.Length >= 3))
        {
            CreateUserButton.SetActive(true);
        }
        else
        {
            CreateUserButton.SetActive(false);
        }
    }
    public void OnClick_JoinRoom()
    {
        RoomOptions ro = new RoomOptions();
        ro.MaxPlayers = 8;
        PhotonNetwork.JoinOrCreateRoom(JoinRoomInputField.text, ro, TypedLobby.Default);
    }
    public void OnClick_CreateRoom()
    {
        RoomOptions ro = new RoomOptions();
        ro.MaxPlayers = 8;
        PhotonNetwork.CreateRoom(CreateRoomInputField.text, ro, null);
    }
    public override void OnJoinedRoom()
    {
        //base.OnJoinedRoom();
        //play game scene
        PhotonNetwork.LoadLevel("Game2D");
    }
}

