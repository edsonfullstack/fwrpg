﻿using System.Collections;
using System.Collections.Generic;
using Database;
using Database.Model;
using Mecanics.Core;
using NSubstitute;
using NUnit.Framework;
using Player.Control;
using UnityEngine;
using UnityEngine.TestTools;


namespace Tests
{
    public class Tests
    {
        // A Test behaves as an ordinary method
        [Test]
        public void TestsSimplePasses()
        {
            // Use the Assert class to test conditions
            Character character = new Character();
            Account account = new Account();
            Mongo mongo = new Mongo();
            int c = mongo.Count(collection: "account", q: "{ 'UserName': 'Set', 'Type' : 'Normal' }", format: account);
            Debug.Log(c);
            Assert.AreEqual(2, c);
        }

        // A UnityTest behaves like a coroutine in Play Mode. In Edit Mode you can use
        // `yield return null;` to skip a frame.
        [UnityTest]
        public IEnumerator TestsWithEnumeratorPasses()
        {
            // Use the Assert class to test conditions.
            // Use yield to skip a frame.
            yield return null;
        }
        [Test]
        public void RollDice()
        {
            var gm = Substitute.For<GameMath>();
            Debug.Log(gm.DiceRoll(1, 4));
            Assert.AreEqual(1, 1);
        }
        [Test]
        public void Player()
        {
            IPlayerControllerPointClick p = Substitute.For<IPlayerControllerPointClick>();
            p.Test().Returns(100);
            Debug.Log(p.Test());
            Assert.AreEqual(100, p.Test());
        }
    }
}
